<%@ page import="org.example.mvc01.entiti.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: oleg
  Date: 02.07.2021
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>hasy</title>
  <style>


    .hasy { /* Круг для часов */
      position: relative;
      width: 100px;
      height: 100px;
      margin: 5em;
      border: 4px solid #48d1cc;
      border-radius: 50%;
    }

    .minutes { /* Минутная стрелка */
      position: absolute; /*Позиционируем относительно .hasy */
      width: 3px; /* Толщина стрелки */
      height: 35px; /* Длина стрелки */
      left: 51px; /*Положение стрелки*/
      top: 16px;
      background: #24C0BA;
      animation: now 1600s infinite steps(1600); /* Скорость и движение стрелки */
      transform-origin: 0 34px; /* Точка относительно которой вращается стрелка */
    }

    .has {
      position: absolute;
      width: 4px;
      height: 28px;
      left: 50px;
      top: 20px;
      background: #009B95;
      animation: nov 43200s infinite steps(43200);
      transform-origin: 0 29px;
    }

    .sekundes {
      position: absolute;
      width: 2px;
      height: 45px;
      left: 51px;
      top: 4px;
      background: #48d1cc;
      animation: nev 60s infinite steps(60);
      transform-origin: 0 45px;
    }

    @keyframes nev {
      100%{
        transform: rotate(360deg); /* Стрелка делает полный круг */
      }
    }

    @keyframes nov {
      100%{
        transform: rotate(360deg);
      }
    }

    @keyframes now {
      100%{
        transform: rotate(360deg);
      }
    }

    .onen, .threen, .sixn, .ninen { /* Цифры на циферблате */
      position: absolute;
      font-weight: bold;
      font-size: 18px;
      color: #48d1cc;
    }

    .onen {
      top: 3px;
      left: 41px;
    }

    .threen {
      top: 43px;
      left: 85px;
    }

    .sixn {
      top: 78px;
      left: 43px;
    }

    .ninen {
      top: 43px;
      left: 5px;
    }

    .sentr { /* Центр циферблата */
      position: absolute;
      width: 15px;
      height: 15px;
      border-radius: 50%;
      top: 42px;
      left: 44px;
      background: #009B95;
    }
  </style>
</head>

<body>

<h5>Тренировка пошла</h5>
<div class="hasy">
  <div class="onen">12</div>
  <div class="threen">3</div>
  <div class="sixn">6</div>
  <div class="ninen">9</div>
  <div class="sekundes"></div>
  <div class="minutes"></div>
  <div class="has"></div>
  <div class="sentr"></div>
</div>

<p>Ув.<%=request.getParameter("name")%> , GO тренироваться !!!! </p>

<p></p>


<%User uzer = new User()
        .setLogin(request.getParameter("name"));
%>


<form action="<c:url value="/fiinish1"/>" method="get">

  </p>
  <button name="button" img src="images/umbrella.gif"><- Закончить тренировку </button>
  <input type="hidden" value='<%=uzer.getLogin()%>' name="login">

</form>



<style>body{background: rgba(95, 160, 137, 0.42)}</style>


</body>
</html>
