package org.example.mvc01.controller;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.example.mvc01.entiti.User;
import org.example.mvc01.service.ServiceUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;

@RestController
@RequiredArgsConstructor
public class ExController {

    @Autowired
    private ServiceUsers service;

    @PostMapping("addUser")
    public Object addUser(@RequestParam("name") String name, @RequestParam("password") String password) throws Exception {
        RedirectView redirect = new RedirectView("http://localhost:8080/newApplicationsTraining_war_exploded/status");
        User user = new User().setLogin(name).setPassword(password).setEntryData(new Date().toString());
        service.Registration(user);
        redirect.setExposeModelAttributes(false);
        return redirect;
    }


}







