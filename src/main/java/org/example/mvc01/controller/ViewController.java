package org.example.mvc01.controller;
import org.example.mvc01.entiti.SessionApp;
import org.example.mvc01.entiti.Subscription;
import org.example.mvc01.entiti.User;
import org.example.mvc01.service.ServiceUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;


@Controller
public class ViewController {

    @Autowired
    private ServiceUsers service;
    @Autowired
    private SessionApp sessionApp;
    static Date dateEnterStart;
    static Date dateEnterFinish;

    @GetMapping("")
    public String mainApp() {
        return "mainMenuApp";}

    @GetMapping("registration")
    public String registrationApp() {
        return "registration";}

    @GetMapping("status")
    public String statusRegistractionApp() {
        return "statusregistraction";
    }

    @PostMapping("app")
    public Object LoginАpplicationApp(@RequestParam("name") String name, @RequestParam("password") String password) throws Exception {
        dateEnterStart=sessionApp.GetTime();
        int overallTime = 0;
        User user = new User().setLogin(name).setPassword(password);

        Map details = service.getAllDetaols(name);
            overallTime = (int) details.get("overallTime");

        if(overallTime==0){
            return MenuForUserWithNotSubscription(user);}

        if(service.Login(user)){
            if(service.checkUserAbonement(user.getLogin())==true){
                return MenuForUserWithSubscription(user) ;
            }else {
                return MenuForUserWithNotSubscription(user);
            }
        }else {
            System.out.println("Нет пользователя "); // можно добаить адресацию
        }return "";}

    @GetMapping("menuTrening")
    public String MenuForUserWithSubscription(User user) {
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("user",user);
        modelMap.put("name",user.getLogin());
        return "menuTrening";}

    @GetMapping("menu")
    public String MenuForUserWithNotSubscription(User user) {
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("user",user);
        modelMap.put("name",user.getLogin());
        return "menupurchases";}

    @PostMapping("tren")
    public String traning(@RequestParam("abonement") Integer abonement, @RequestParam("login") String login,@RequestParam("Password") String password) throws Exception {

        System.out.println("То что должно зайти "+login);

        User user = new User().setLogin(login);

        System.out.println("Заходим по логинке что у клиента нет абонемента , Закодит клиент _"+login);

        Subscription subscription = extracted(abonement); // получил абонемент который выбрал клиент

        System.out.println("Клиент покупает абонемент");

        boolean statusputSub = service.putSubscriptionTable(subscription);// добавили абонемент в базу данных subscription

        System.out.println("Добавляем его в базу данных статус добавления ="+statusputSub);
        if(!statusputSub){throw new Exception("Покупка абонемента была неуспешна ");} // проверяем статус добавления абонемента в базу данных subscription

        boolean statusputUserandSub = service.putUserandSub(login,subscription); // добавляем данные в userssubscription
        System.out.println("Добавляем данные в userssubscription статус ="+statusputUserandSub);

        dateEnterStart=sessionApp.GetTime();
        boolean statusputVisitTable = service.putVisitTable(login, String.valueOf(dateEnterStart),subscription);
        System.out.println("Добавляем данные в таблицу Visit, статус ="+statusputVisitTable);

        return trensz(login);
    }
    private Subscription extracted(Integer abonement) {
        Subscription subscription = null;
        if(abonement==120){subscription=new Subscription().setNames("Абонемент на 120 часов").setPrice("1200").setTime(120);}
        if(abonement.equals(100)){subscription=new Subscription().setNames("Абонемент на 100 часов").setPrice("1000").setTime(100);}
        if(abonement.equals(80)){subscription=new Subscription().setNames("Абонемент на 80 часов").setPrice("800").setTime(80);}
        subscription.setDatePurchase(new Date().toString());
        return subscription;
    }

    @GetMapping("trensz")
    public String trensz(String login) {
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("login",login);
        return "menuTrening1";}

    @PostMapping("fiinish")
    public String fiinish(@RequestParam("login") String login) throws SQLException {
        int overallTime = 0;
        Map details = service.getAllDetaols(login);
        if(details!=null){
            overallTime = (int) details.get("overallTime");}
        dateEnterFinish = sessionApp.GetTime();
        double minutes = ((dateEnterFinish.getTime()-dateEnterStart.getTime()) / 1000)  / 60;
        overallTime = (int) (overallTime-minutes);

        int time = (int) (overallTime-minutes);

        service.selectNewDetails(login,time,dateEnterFinish);
        return "fiinish";
    }

    @GetMapping("fiinish1")
    public String fiinish1(@RequestParam("login") String login) throws SQLException {


        dateEnterFinish = sessionApp.GetTime();
        double minutes = ((dateEnterFinish.getTime()-dateEnterStart.getTime()) / 1000)  / 60;

        String idUser;
        String idSubscription;
        int overallTime = 0;

        Map details = service.getAllDetaols(login);
        System.out.println("Смотрим что в нашем массиве "+details);

            idUser = (String) details.get("idUser");
            idSubscription = (String) details.get("idSubscription");
            overallTime = (int) details.get("overallTime");

        int time = (int) (overallTime-minutes);
        System.out.println("details "+details);
        System.out.println("overallTime "+time);

        service.getPastAllDetails(dateEnterStart,dateEnterFinish,time,login);

        return "fiinish1";
    }




}
