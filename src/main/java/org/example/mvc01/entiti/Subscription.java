package org.example.mvc01.entiti;



import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Subscription {

    String names;
    String price;
    Integer time;
    String DatePurchase;

}
