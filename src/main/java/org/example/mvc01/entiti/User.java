package org.example.mvc01.entiti;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Properties;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

public class User extends Properties {

    private String Login;
    private String password;
    private String entryData;



}
