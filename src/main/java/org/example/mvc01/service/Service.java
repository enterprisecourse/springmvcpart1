package org.example.mvc01.service;

import org.example.mvc01.entiti.Subscription;
import org.example.mvc01.entiti.User;

public interface Service {

    void Registration(User user) throws Exception;
    boolean Login(User user) throws Exception;
    boolean putUserandSub(String login, Subscription subscription);
}
