package org.example.mvc01.service;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.example.mvc01.dao.DaoUser;
import org.example.mvc01.entiti.Subscription;
import org.example.mvc01.entiti.User;
import org.example.mvc01.entiti.ValidatorApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;


@RequiredArgsConstructor
@Component
public class ServiceUsers implements Service {

    @Autowired
    private final DaoUser daoUser;
    @Autowired
    private final ValidatorApp validatorApp;



    @Override
    public void Registration(User user) throws Exception {
        if (validatorApp.CheckUser(user) == false) {
            throw new Exception("Некорректный логин или пароль"); // проверяем на null}
       }
        if (daoUser.searchUserDataBase(user) == false) {
            throw new Exception("Пользователь с таким логином уже есть"); // проверяем на дубль}
        }
        daoUser.Registration(user);
    }


    @SneakyThrows
    @Override
    public boolean Login(User user) throws Exception {

        if (daoUser.searchUserDataBase(user) == false) {
            return true;
        }
        return false;

    }


    public boolean putSubscriptionTable(Subscription subscription) {

        if(daoUser.putSubscriptionTable(subscription)!=true){return false;}

        return true;
    }

    public boolean putUserandSub(String login, Subscription subscription) {

        daoUser.putUserandSub(login,subscription);
        return false;
    }

    public boolean putVisitTable(String login ,String enterAfter,Subscription subscription) {
        daoUser.putVisitTable(login,enterAfter,subscription);

        return true;
    }



    public boolean checkUserAbonement(String login) throws SQLException {
        return daoUser.checkUserAbonement(login);
    }

    public Map getAllDetaols(String login) throws SQLException {

        return daoUser.getAllInformationFirst(login);
    }

    public void selectNewDetails(String login, int overallTime, Date dateEnterStart) throws SQLException {
        daoUser.selectNewInformatio(login,overallTime,dateEnterStart);
    }


    public void getPastAllDetails(Date dateEnterStart, Date dateEnterFinish, int time, String login) throws SQLException {
        daoUser.getAllInformationSecond(dateEnterStart,dateEnterFinish,time,login);
    }
}
