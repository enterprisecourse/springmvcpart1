package org.example.mvc01.dao;
import lombok.RequiredArgsConstructor;
import org.example.mvc01.entiti.Subscription;
import org.example.mvc01.entiti.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Repository
@RequiredArgsConstructor
@Component
public class DaoUsers implements DaoUser {

    @Autowired
    DataSource dataSource;

    @Override // Регистрация пользователя
    public void Registration(User user) {
        System.out.println("DaoUsers.Registration");

        String sql = "INSERT INTO users (user_name,user_password,user_entryData) VALUES\n" +
                "(?,?,?)";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEntryData());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    @Override
    public boolean searchUserDataBase(User user) throws SQLException {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement1 = connection.prepareStatement(
                    "SELECT user_name , user_password FROM users;");
            ResultSet resultSet = statement1.executeQuery();

            while (resultSet.next()) {
                if (user.getLogin().equals(resultSet.getString("user_name"))) {
                    return false;
                }
            }
            return true;
        }

    }
    @Override
    public boolean putSubscriptionTable(Subscription subscription) {

        System.out.println("subscription.getNames()" + subscription.getNames());
        System.out.println("subscription.getTime()" + subscription.getTime());
        System.out.println("subscription.getPrice()" + subscription.getPrice());
        System.out.println("subscription.getDatePurchase()" + subscription.getDatePurchase());


        String sql = "INSERT INTO subscription (subscription_name,subscription_time,subscription_price,subscription_datepurchase) VALUES\n" +
                "(?,?,?,?)";

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, subscription.getNames());
            preparedStatement.setInt(2, subscription.getTime());
            preparedStatement.setString(3, subscription.getPrice());
            preparedStatement.setString(4, subscription.getDatePurchase());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            return false;
        }

        return true;


    }
    @Override
    public void putUserandSub(String login, Subscription subscription) {

        System.out.println("Login " + login);
        System.out.println("getDatePurchase " + subscription.getDatePurchase());


        String sql = "INSERT INTO userssubscription (userssubscription_purchase_date,userssubscription_state,id_users,id_subscription)\n" +
                "VALUES (?,?,(SELECT id_users FROM users WHERE user_name=?),(SELECT id_subscription FROM subscription WHERE subscription_datepurchase=?));";

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, subscription.getDatePurchase());
            preparedStatement.setString(2, "active");
            preparedStatement.setString(3, login);
            preparedStatement.setString(4, subscription.getDatePurchase());
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
        }


    }
    @Override
    public void putVisitTable(String login, String enterAfter, Subscription subscription) {

        String sql = "INSERT INTO visit (date_entry,date_exite,overall_time,id_subscription,id_users)\n" +
                "VALUES (?,?,?,\n" +
                "  (SELECT id_subscription FROM userssubscription as t1 join users as t2 on t1.id_users = t2.id_users where t2.user_name = ?),\n" +
                "  (SELECT id_users FROM users WHERE user_name = ?));";

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, enterAfter);
            preparedStatement.setString(2, "0");
            preparedStatement.setInt(3, subscription.getTime() * 60);
            preparedStatement.setString(4, login);
            preparedStatement.setString(5, login);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
        }
    }
    @Override
    public boolean checkUserAbonement(String login) throws SQLException {


        ArrayList subscriptions = new ArrayList();

        String id = null;

        try (Connection connection1 = dataSource.getConnection()) {
            PreparedStatement statement1 = connection1.prepareStatement(
                    "SELECT id_users FROM users WHERE user_name=?;\n");
            statement1.setString(1, login);

            ResultSet resultSet = statement1.executeQuery();

            while (resultSet.next()) {
                id = resultSet.getString("id_users");
            }
        }
        if (id != null) {

            int UserId = Integer.parseInt(id);
            try (Connection connection2 = dataSource.getConnection()) {
                PreparedStatement statement2 = connection2.prepareStatement(
                        "SELECT id_subscription FROM userssubscription WHERE id_users=?;");
                statement2.setInt(1, UserId);
                ResultSet resultSet = statement2.executeQuery();
                while (resultSet.next()) {
                    String ab = resultSet.getString("id_subscription");
                    subscriptions.add(ab);
                }
            }
            if (subscriptions.size() > 0) {
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

    @Override
    public Map getAllInformationFirst(String login) throws SQLException {

        Map<Object,Object> tables= new HashMap<>();
        String idUser = null;
        String idSubscription = null;
        int overallTime=0;

        try (Connection connection1 = dataSource.getConnection()) {
            PreparedStatement statement1 = connection1.prepareStatement(
                    "SELECT id_users FROM users WHERE user_name=?;\n");
            statement1.setString(1, login);

            ResultSet resultSet = statement1.executeQuery();

            while (resultSet.next()) {
                idUser = resultSet.getString("id_users");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        System.out.println("id "+idUser);

        if (idUser != null) {

            int UserId = Integer.parseInt(idUser);

            try (Connection connection2 = dataSource.getConnection()) {
                PreparedStatement statement2 = connection2.prepareStatement(
                        "SELECT overall_time, id_subscription FROM visit WHERE id_users=?;");
                statement2.setInt(1, UserId);
                ResultSet resultSet = statement2.executeQuery();
                while (resultSet.next()) {
                    overallTime=Integer.parseInt(resultSet.getString("overall_time"));
                    idSubscription=resultSet.getString("id_subscription");
                }
            }

            tables.put("idUser",idUser);
            tables.put("idSubscription",idSubscription);
            tables.put("overallTime",overallTime);

            System.out.println("tables "+tables);

//            try (Connection connection3 = dataSource.getConnection()) {
//                PreparedStatement statement3= connection3.prepareStatement(
//                        "DELETE FROM visit WHERE id_users = ?");
//                statement3.setInt(1, UserId);
//                statement3.executeUpdate();
//    }


    }

return tables;

}

    @Override
    public void selectNewInformatio(String login, int overallTime, Date dateEnterFinish) throws SQLException {

        String idLogin = null;
        String data = String.valueOf(dateEnterFinish);


        try (Connection connection1 = dataSource.getConnection()) {
            PreparedStatement statement1 = connection1.prepareStatement(
                    "SELECT id_users FROM users WHERE user_name=?;\n");
            statement1.setString(1, login);
            ResultSet resultSet = statement1.executeQuery();
            while (resultSet.next()) {
                idLogin = resultSet.getString("id_users");}
        } catch (SQLException throwables) {
            throwables.printStackTrace();}

        int iD = Integer.parseInt(idLogin);
        System.out.println("TASK 2");
        try (Connection connection2 = dataSource.getConnection()) {
            PreparedStatement statement2 = connection2.prepareStatement(
                    "UPDATE visit SET overall_time = ? WHERE id_users = ?;");
            statement2.setInt(1,overallTime);
            statement2.setInt(2,iD);
            statement2. executeUpdate();
        }

        try (Connection connection3 = dataSource.getConnection()) {
            PreparedStatement statement3 = connection3.prepareStatement(
                    "UPDATE visit SET date_exite = ? WHERE id_users = ?;");
            statement3.setString(1,String.valueOf(dateEnterFinish));
            statement3.setInt(2,iD);
            statement3. executeUpdate();
        }


}

    @Override
    public void getAllInformationSecond(Date dateEnterStart, Date dateEnterFinish, int time, String login) throws SQLException {

        String idLogin = null;
        try (Connection connection1 = dataSource.getConnection()) {
            PreparedStatement statement1 = connection1.prepareStatement(
                    "SELECT id_users FROM users WHERE user_name=?;\n");
            statement1.setString(1, login);
            ResultSet resultSet = statement1.executeQuery();
            while (resultSet.next()) {
                idLogin = resultSet.getString("id_users");}
        } catch (SQLException throwables) {
            throwables.printStackTrace();}

        int iDUser = Integer.parseInt(idLogin);


        try (Connection connection2 = dataSource.getConnection()) {
            PreparedStatement statement2 = connection2.prepareStatement(
                    "UPDATE visit SET overall_time = ? WHERE id_users = ?;");
            statement2.setInt(1,time);
            statement2.setInt(2,iDUser);
            statement2. executeUpdate();
        }

        try (Connection connection3 = dataSource.getConnection()) {
            PreparedStatement statement3 = connection3.prepareStatement(
                    "UPDATE visit SET date_entry = ? WHERE id_users = ?;");
            statement3.setString(1,String.valueOf(dateEnterStart));
            statement3.setInt(2,iDUser);
            statement3. executeUpdate();
        }

        try (Connection connection4 = dataSource.getConnection()) {
            PreparedStatement statement4 = connection4.prepareStatement(
                    "UPDATE visit SET date_exite = ? WHERE id_users = ?;");
            statement4.setString(1,String.valueOf(dateEnterFinish));
            statement4.setInt(2,iDUser);
            statement4. executeUpdate();
        }




    }


}



