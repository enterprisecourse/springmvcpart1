package org.example.mvc01.dao;

import org.example.mvc01.entiti.Subscription;
import org.example.mvc01.entiti.User;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

public interface DaoUser {
     void Registration(User user);
     boolean searchUserDataBase(User user) throws SQLException;
    boolean putSubscriptionTable(Subscription subscription);
    void putUserandSub(String login, Subscription subscription);
    void putVisitTable(String login, String enterAfter,Subscription subscription);
    boolean checkUserAbonement(String login) throws SQLException;
    Map getAllInformationFirst(String login) throws SQLException;
    void selectNewInformatio(String login, int overallTime, Date dateEnterStart) throws SQLException;
    void getAllInformationSecond(Date dateEnterStart, Date dateEnterFinish, int time, String login) throws SQLException;
}
